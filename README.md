# POC Spring Data MongoDB

Project used to experiment on [Spring Data MongoDB](http://projects.spring.io/spring-data-mongodb/).

# Getting Started

Project is developed on [Spring Boot](https://projects.spring.io/spring-boot/) and its initial structure has been generated using [Spring Initializr](http://start.spring.io/).

It requires Java 8 and it is built using [Maven 3](https://maven.apache.org/).

System uses an embedded MongoDB instance provided by [`de.flapdoodle.embed.mongo`](http://flapdoodle-oss.github.io/de.flapdoodle.embed.mongo/).

Since System is developed on `Spring Boot` one might use following Maven command to boot System in an embedded Tomcat instance:

    $ mvn spring-boot:run

After System is up and running REST API is exposed under base path `/api/rest/v1`. Example:

    $ curl -i -X GET "http://localhost:8080/api/rest/v1"
    HTTP/1.1 200 
    Content-Type: application/hal+json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Fri, 02 Dec 2016 19:54:41 GMT
    
    {
      "_links" : {
        "people" : {
          "href" : "http://localhost:8080/api/rest/v1/people{?page,size,sort}",
          "templated" : true
        },
        "profile" : {
          "href" : "http://localhost:8080/api/rest/v1/profile"
        }
      }
    }

And then one might list [pre-initialized People instances](src/main/java/br/fcv/poc_spring_data_mongodb/SampleDataCLR.java) via URL `/api/rest/v1/people`. Example:

    $ curl -i -X GET "http://localhost:8080/api/rest/v1/people"
    HTTP/1.1 200 
    Content-Type: application/hal+json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Fri, 02 Dec 2016 20:00:14 GMT
    
    {
      "_embedded" : {
        "people" : [ {
          "name" : "John Doe",
          "birthdate" : "1980-10-27",
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/people/5841d0197e66e2479f58b191"
            },
            "person" : {
              "href" : "http://localhost:8080/api/rest/v1/people/5841d0197e66e2479f58b191"
            }
          }
        }, {
          "name" : "Charles Dickens",
          "birthdate" : "1912-02-07",
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/people/5841d0197e66e2479f58b192"
            },
            "person" : {
              "href" : "http://localhost:8080/api/rest/v1/people/5841d0197e66e2479f58b192"
            }
          }
        }, {
          "name" : "Wolfgang Amadeus Mozart",
          "birthdate" : "1756-01-27",
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/people/5841d0197e66e2479f58b193"
            },
            "person" : {
              "href" : "http://localhost:8080/api/rest/v1/people/5841d0197e66e2479f58b193"
            }
          }
        } ]
      },
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/api/rest/v1/people"
        },
        "profile" : {
          "href" : "http://localhost:8080/api/rest/v1/profile/people"
        }
      },
      "page" : {
        "size" : 20,
        "totalElements" : 3,
        "totalPages" : 1,
        "number" : 0
      }
    }

Or store a new Person entity:

    $ curl -i -X POST \
    > -H "Content-Type: application/json" \
    > -d '{"name": "Albert Einstein", "birthdate": "1879-03-14"}' \
    > "http://localhost:8080/api/rest/v1/people/"
    HTTP/1.1 201 
    Location: http://localhost:8080/api/rest/v1/people/5841d5ab7e66e24b68a452f9
    Content-Type: application/hal+json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Fri, 02 Dec 2016 20:12:27 GMT
    
    {
      "name" : "Albert Einstein",
      "birthdate" : "1879-03-14",
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/api/rest/v1/people/5841d5ab7e66e24b68a452f9"
        },
        "person" : {
          "href" : "http://localhost:8080/api/rest/v1/people/5841d5ab7e66e24b68a452f9"
        }
      }
    }
