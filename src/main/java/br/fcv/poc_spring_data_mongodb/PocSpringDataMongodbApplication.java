package br.fcv.poc_spring_data_mongodb;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static java.time.Month.*;
import static java.util.Arrays.asList;

@SpringBootApplication
public class PocSpringDataMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocSpringDataMongodbApplication.class, args);
	}

	@Bean
	public Logger getLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(ip.getMember().getDeclaringClass());
	}

	@Bean
	public Module newJavaTimeModule() {
		return new JavaTimeModule();
	}

}
