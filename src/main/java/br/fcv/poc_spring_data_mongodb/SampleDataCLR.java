package br.fcv.poc_spring_data_mongodb;

import org.slf4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static java.time.Month.FEBRUARY;
import static java.time.Month.JANUARY;
import static java.time.Month.OCTOBER;
import static java.util.Arrays.asList;

@Component
class SampleDataCLR implements CommandLineRunner {

	private final PersonRepository personRepository;
	private final Logger logger;

	public SampleDataCLR(PersonRepository personRepository, Logger logger) {
		this.personRepository = personRepository;
		this.logger = logger;
	}

	@Override
	public void run(String... args) throws Exception {

		List<Person> people = personRepository.save(asList(new Person("John Doe", LocalDate.of(1980, OCTOBER, 27)),
				new Person("Charles Dickens", LocalDate.of(1912, FEBRUARY, 7)),
				new Person("Wolfgang Amadeus Mozart", LocalDate.of(1756, JANUARY, 27))));

		people.forEach(person -> logger.info("{}", person));
	}
}
