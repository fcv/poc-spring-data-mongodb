package br.fcv.poc_spring_data_mongodb;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.Month;

import static java.time.Month.OCTOBER;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PeopleEndpointTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private PersonRepository personRepository;

	private Person john;

	@Before
	public void initData() {

		personRepository.deleteAll();
		john = personRepository.save(new Person("John Doe", LocalDate.of(1980, OCTOBER, 15)));;
	}

	@Test
	public void testEntityJsonFormat() throws Exception {

		String id = john.getId();

		mvc.perform(get("/api/rest/v1/people/{id}", id).accept(APPLICATION_JSON_UTF8))
			.andExpect(status().isOk())
			.andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
			.andExpect(jsonPath("$.name", is("John Doe")))
			.andExpect(jsonPath("$.birthdate", is("1980-10-15")))
			;
	}

}
